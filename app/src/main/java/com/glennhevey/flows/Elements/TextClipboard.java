package com.glennhevey.flows.Elements;

import android.content.ClipboardManager;
import android.content.Context;

/**
 * Created by glenn on 13/01/2016.
 * Grabs text from the OS clipboardManager
 */
public class TextClipboard implements IElement {
    int number = 0;
    Object outputParameter = null;
    Object inputParamater = null;

    @Override
    public String getName() {
        return "Get Clipboard";
    }

    @Override
    public String getDescription() {
        return "Get Text that is in the clipboard of the device";
    }

    @Override
    public String[] getInputType() {
        return new String[] {"null"};
    }

    @Override
    public String[] getOutputType() {
        return new String[] {"CharSequence"};
    }

    @Override
    public void setInputNumber(int number) {
        this.number = number;
    }

    @Override
    public int getInputNumber() {
        return number;
    }

    @Override
    public void run(Context con) {
        ClipboardManager clipboardManager = (ClipboardManager) con.getSystemService(Context.CLIPBOARD_SERVICE);
        outputParameter = clipboardManager.getPrimaryClip().getItemAt(0).getText();

    }

    @Override
    public void setInputParamater(Object obj) {
        inputParamater = obj;
    }

    @Override
    public Object getOutputParameter() {

        return outputParameter;
    }


}
