package com.glennhevey.flows.Elements;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by glenn on 14/01/2016.
 * Outputs passed in ntext to an alert
 */
public class Alert implements IElement {
    int number = 0;
    Object inputParameter = null;
    Object outputParameter = null;

    @Override
    public String getName() {
        return "Text - Alert Dialog";
    }

    @Override
    public String getDescription() {
        return "Outputs the previous elements output to a Dialog Box";
    }

    @Override
    public String[] getInputType() {
        return new String[] {"String"};
    }

    @Override
    public String[] getOutputType() {
        return new String[] {"null"};
    }

    @Override
    public void setInputNumber(int number) {
        this.number = number;
    }

    @Override
    public int getInputNumber() {
        return number;
    }

    @Override
    public void setInputParamater(Object obj) {
        inputParameter = obj;
    }

    @Override
    public Object getOutputParameter() {
        return null;
    }

    @Override
    public void run(Context con) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(con);

        alertDialogBuilder
            .setTitle("Your Clipboard")
            .setMessage((String) inputParameter)
            .setNeutralButton("Ok",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // if this button is clicked, just close
                    // the dialog box and do nothing
                    dialog.cancel();
                }
            });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        outputParameter = "done";

    }
}
