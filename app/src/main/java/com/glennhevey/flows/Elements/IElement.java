package com.glennhevey.flows.Elements;

import android.content.Context;

/**
 * Created by glenn on 13/01/2016.
 */
public interface IElement {

    String getName();
    String getDescription();
    String[] getInputType();
    String[] getOutputType();
    void setInputNumber(int number);
    int getInputNumber();
    void run(Context context);
    void setInputParamater(Object obj);
    Object getOutputParameter();
}
