package com.glennhevey.flows;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.glennhevey.flows.Adapters.ElementAdapter;
import com.glennhevey.flows.Controllers.ElementController;
import com.glennhevey.flows.Controllers.FlowsController;


public class ElementListActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element_list);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.element_list);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        RecyclerView.Adapter mAdapter = new ElementAdapter(ElementController.getAllElements());
        mRecyclerView.setAdapter(mAdapter);

        FlowsController flow = new FlowsController(ElementListActivity.this);

        flow.runFlow(new String[] {"TextClipboard","Alert"});
    }
}
