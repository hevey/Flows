package com.glennhevey.flows.Controllers;

import com.glennhevey.flows.Elements.IElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenn on 14/01/2016.
 * Controller for returning Element Objects
 */
public class ElementController {

    private enum elementList {
        TextClipboard,
        Alert
    }

    public static List<IElement> getAllElements() {
        List<IElement> elements = new ArrayList<>();

        for (elementList name:
                elementList.values()) {
            Class classDefinition;
            try {
                classDefinition = Class.forName("com.glennhevey.flows.Elements." + name);

                elements.add((IElement)classDefinition.newInstance());

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }
        return elements;
    }

    public static List<IElement> getElements(String[] names) {
        List<IElement> elements = new ArrayList<>();

        for (String name:
                names) {
            Class classDefinition;
            try {
                classDefinition = Class.forName("com.glennhevey.flows.Elements." + name);

                IElement element = (IElement)classDefinition.newInstance();
                element.setInputNumber(elements.size());
                elements.add(element);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }
        return elements;
    }
}
