package com.glennhevey.flows.Controllers;

import android.content.Context;

import com.glennhevey.flows.Elements.IElement;
import com.google.gson.Gson;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


/**
 * Created by glenn on 14/01/2016.
 * Runs the list of passed in elements together.
 */
public class FlowsController {

    Context mContext;
    Object data = null;

    public FlowsController(Context context) {
        mContext = context;
    }

    public void runFlow(String[] elementNames) {
        List<IElement> elements = ElementController.getElements(elementNames);

        for (IElement element :
                elements) {
            element.setInputParamater(data);
            element.run(mContext);

            data = element.getOutputParameter();

        }

    }

    public void saveFlow(String[] elementNames, String name) {
        Gson gson = new Gson();

        String json = gson.toJson(elementNames);

        FileOutputStream outputStream;

        try {
            outputStream = mContext.openFileOutput("Flows\\" + name + ".json", Context.MODE_PRIVATE);
            outputStream.write(json.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String[] openFlow(String name) {
        FileInputStream fis = null;
        Gson gson = null;
        String json = null;
        try {
            fis = mContext.openFileInput("Flows\\" + name + ".json");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            json = sb.toString();
            gson = new Gson();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert gson != null;
        return gson.fromJson(json, String[].class);

    }
}
