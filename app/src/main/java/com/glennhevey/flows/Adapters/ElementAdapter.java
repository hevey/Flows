package com.glennhevey.flows.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glennhevey.flows.Elements.IElement;
import com.glennhevey.flows.R;

import java.util.List;

/**
 * Created by glenn on 13/01/2016.
 */
public class ElementAdapter extends RecyclerView.Adapter<ElementAdapter.ViewHolder> {

    private List<IElement> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mName;
        public TextView mDescription;
        public ViewHolder(View v) {
            super(v);
            mName = (TextView) v.findViewById(R.id.name);
            mDescription = (TextView) v.findViewById(R.id.description);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ElementAdapter(List<IElement> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public ElementAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ElementAdapter.ViewHolder holder, int position) {

        holder.mName.setText(mDataset.get(position).getName());
        holder.mDescription.setText(mDataset.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
